
import java.util.Scanner;

    public class main {
        public static void main(String[] args) {
            System.out.println("Задание 1: ");
            Task1();
            System.out.println("Задание 2: ");
            Task2();
            System.out.println("Задание 3: ");
            Task3();
            System.out.println("Задание 4: ");
            Task4();
            System.out.println("Задание 5: ");
            Task5();
        }

        private static void Task1() {
            System.out.println("Введите число 1, 2 или 3: ");
            Scanner inputFigure = new Scanner(System.in);
            int i = inputFigure.nextInt();

            if (i > 0 && i < 4) {
                System.out.print("Пользователь ввёл: ");
                System.out.println(i);
            } else {
                System.out.println("Неверный ввод!");
            }

            switch (i) {
                case 1:
                case 2:
                case 3:
                    System.out.println("Пользователь ввёл: ");
                    System.out.println(i);
                    break;
                default:
                    System.out.println("Неверный ввод! ");
                    System.out.println(i);
                    break;
            }
        }

        private static void Task2() {
            int i = 5;
            do {
                System.out.print(i--);
            }while (i!=0);
            System.out.println();
        }

        private static void Task3() {
            for (int i=1;i <=10;i++) {
                System.out.println("3*" + (i) + "=" + (3 * i));
            }
            System.out.println();
        }

        private static void Task4() {
            System.out.println("Среднее значение суммы чисел от 1 до 100 равно: ");
            int sum=0;
            int count = 0;

            for (int i= 1; i<=100; i++, count++) {
                sum += i;
            }
            System.out.println(sum/count);
        }

        private static void Task5() {
            int [] array = {5,2,4,8,88,22,10};
            int max = array[0];
            for (int i = 1; i < array.length;i++) {
                if (array[i] > max) {
                    max = array[i];
                }
            }
            System.out.println("Максимальное значение чисел из массива {5,2,4,8,88,22,10} равно: " + (max));
        }
    }


